# Canvas-Path-2
Path

<div>
<canvas id="c4" width="600" height = "200" style="border:solid 1px #000000;"></canvas>
<div>
  <button onclick="Vertical_2px_Red();return true;">Vertical 2px Red line</button>
  <button onclick="Vertical_1px_Blue();return true;">Vertical 1px Blue line</button>
  <button onclick="Horizontal_2px_Green();return true;">Horizontal 2px Green line</button>
  <button onclick="Clear_line();return true;">Erase Everything</button>
</div> 
</div>

<script>
var c4 = document.getElementById("c4");
var c4_context = c4.getContext("2d");

function Vertical_2px_Red() {
c4_context.beginPath();
c4_context.moveTo(300, 10);
c4_context.lineTo(300, 190);
c4_context.strokeStyle = "Red";
c4_context.stroke();
}

function Vertical_1px_Blue() {
c4_context.beginPath();
c4_context.moveTo(350.5, 10);
c4_context.lineTo(350.5, 190);
c4_context.strokeStyle = "Blue";
c4_context.stroke();
}

function Horizontal_2px_Green() {
c4_context.beginPath();
c4_context.moveTo(100, 100);
c4_context.lineTo(500, 100);
c4_context.strokeStyle = "Green";
c4_context.stroke();
}

function Clear_line() {
c4_context.clearRect(1, 1, 600, 190);
}
</script>
